'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('siswas', {
      siswaId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      nisn: {
        type: Sequelize.INTEGER
      },
      nama: {
        type: Sequelize.TEXT
      },
      jeniskelamin: {
        type: Sequelize.TEXT
      },
      tempatlahir: {
        type: Sequelize.TEXT
      },
      tanggallahir: {
        type: Sequelize.DATEONLY
      },
      tahunmasuk: {
        type: Sequelize.INTEGER
      },
      semester: {
        type: Sequelize.INTEGER
      },
      kelas: {
        type: Sequelize.TEXT
      },
      jurusan: {
        type: Sequelize.TEXT
      },
      noteleponsiswa: {
        type: Sequelize.TEXT
      },
      namaayah: {
        type: Sequelize.TEXT
      },
      namaibu: {
        type: Sequelize.TEXT
      },
      pekerjaanayah: {
        type: Sequelize.TEXT
      },
      pekerjaanibu: {
        type: Sequelize.TEXT
      },
      notelepon: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('siswas');
  }
};