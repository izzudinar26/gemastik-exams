'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('soals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      mata_pelajaran: {
        type: Sequelize.STRING
      },
      kelas: {
        type: Sequelize.STRING
      },
      token: {
        type: Sequelize.TEXT
      },
      soal: {
        type: Sequelize.TEXT
      },
      soalgambar: {
        type: Sequelize.TEXT
      },
      jawaban0: {
        type: Sequelize.STRING
      },
      jawaban0gambar: {
        type: Sequelize.TEXT
      },
      jawaban1: {
        type: Sequelize.STRING
      },
      jawaban1gambar: {
        type: Sequelize.TEXT
      },
      jawaban2: {
        type: Sequelize.STRING
      },
      jawaban2gambar: {
        type: Sequelize.TEXT
      },
      jawaban3: {
        type: Sequelize.STRING
      },
      jawaban3gambar: {
        type: Sequelize.TEXT
      },
      answer: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('soals');
  }
};