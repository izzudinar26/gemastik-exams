'use strict';
module.exports = (sequelize, DataTypes) => {
  const soal = sequelize.define('soal', {
    mata_pelajaran: DataTypes.STRING,
    kelas: DataTypes.STRING,
    token: DataTypes.TEXT,
    soal: DataTypes.TEXT,
    soalgambar: DataTypes.TEXT,
    jawaban0: DataTypes.STRING,
    jawaban0gambar: DataTypes.TEXT,
    jawaban1: DataTypes.STRING,
    jawaban1gambar: DataTypes.TEXT,
    jawaban2: DataTypes.STRING,
    jawaban2gambar: DataTypes.TEXT,
    jawaban3: DataTypes.STRING,
    jawaban3gambar: DataTypes.TEXT,
    answer: DataTypes.STRING
  }, {});
  soal.associate = function(models) {
    // associations can be defined here
  };
  return soal;
};