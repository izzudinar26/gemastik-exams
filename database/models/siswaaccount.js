'use strict';

module.exports = (sequelize, DataTypes) => {
  const siswaAccount = sequelize.define('siswaAccount', {
    nisn: DataTypes.INTEGER,
    nama: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  siswaAccount.associate = function (models) {
    // associations can be defined here
  };
  return siswaAccount;
};