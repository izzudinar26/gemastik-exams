'use strict';


require("./siswaaccount")

module.exports = (sequelize, DataTypes) => {
  const siswa = sequelize.define('siswa', {
    siswaId: { type: DataTypes.INTEGER, primaryKey: true },
    nisn: DataTypes.INTEGER,
    nama: DataTypes.TEXT,
    jeniskelamin: DataTypes.TEXT,
    tempatlahir: DataTypes.TEXT,
    tanggallahir: DataTypes.DATEONLY,
    tahunmasuk: DataTypes.INTEGER,
    semester: DataTypes.INTEGER,
    kelas: DataTypes.TEXT,
    jurusan: DataTypes.TEXT,
    noteleponsiswa: DataTypes.TEXT,
    namaayah: DataTypes.TEXT,
    namaibu: DataTypes.TEXT,
    pekerjaanayah: DataTypes.TEXT,
    pekerjaanibu: DataTypes.TEXT,
    notelepon: DataTypes.TEXT
  }, {
    freezeTableName: true,
    timestamps: false
  });
  siswa.associate = function (models) {
    // associations can be defined here
  };
  return siswa;
};