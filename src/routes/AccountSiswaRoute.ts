import { Router } from "express";
import AccountSiswaService from "../services/AccountSiswaServices";
import { accountupload } from "../middleware/accountupploader";

class AccountSiswaRoute {
  router: Router;
  constructor() {
    this.router = Router();
    this.routes();
  }
  routes() {
    //crud method
    this.router.get("/", AccountSiswaService.get);
    this.router.get("/:id", AccountSiswaService.getId);
    this.router.post(
      "/",
      accountupload.single("account"),
      AccountSiswaService.store
    );
    this.router.put("/:id", AccountSiswaService.update);
    this.router.delete("/:id", AccountSiswaService.destroy);

    //auth method
    this.router.post("/authentication", AccountSiswaService.Authentication);
  }
}

export const accountSiswaRoute = new AccountSiswaRoute().router;
