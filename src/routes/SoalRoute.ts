import { Router } from "express";
import { upload } from "../middleware/uploader";
import SoalService from "../services/soalServices";
import soalServices from "../services/soalServices";

class SoalRoute {
  router: Router;
  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post("/upload", upload.single("soal"), soalServices.store);
    this.router.get("/", soalServices.get);
  }
}

export const soalRoute = new SoalRoute().router;
