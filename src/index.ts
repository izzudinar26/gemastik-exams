import express, { Application } from "express";
import bodyParser from "body-parser";
import { soalRoute, accountSiswaRoute } from "./routes";
import morgan from "morgan";
require("dotenv").config();

class App {
  public app: Application;
  constructor() {
    this.app = express();
    this.plugins();
    this.route();
  }

  plugins() {
    this.app.use(morgan("dev"));
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
  }

  route() {
    this.app.use("/api/Soal", soalRoute);
    this.app.use("/api/siswa-account", accountSiswaRoute);
  }
}

const port = process.env.APP_PORT || 8081;
const app: Application = new App().app;
app.listen(port, () => {
  console.log(`Running on http://localhost:${port}`);
});
