import path from "path";
import xlsx from "node-xlsx";
const db: any = require("../../database/models");

class SoalService {
  get(req: any, res: any) {
    return res.send("hello");
  }

  async store(req: any, res: any) {
    if (req.file) {
      let { mata_pelajaran, kelas } = req.body;

      //parsing xlsx document
      const token = Math.random().toString(36).substr(2, 5); //generate token
      const xls = xlsx.parse(
        path.join(__dirname + "../../../storage/soal/" + req.file.filename)
      );
      let data: any[] = xls[0].data;

      //remove first element index
      data.shift();

      //insert data
      try {
        for (let doc of data) {
          db.soal.create({
            token: token,
            mata_pelajaran: mata_pelajaran,
            soal: doc[0],
            soalgambar: doc[1],
            jawaban0: doc[2],
            jawaban0gambar: doc[3],
            jawaban1: doc[4],
            jawaban1gambar: doc[5],
            jawaban2: doc[6],
            jawaban2gambar: doc[7],
            jawaban3: doc[8],
            jawaban3gambar: doc[9],
            answer: doc[10],
          });
        }
      } catch (error) {
        console.log(error);
        return res.send(500);
      }

      return res.send(200);
    }
    return res.send({ response: "Tidak ditemukan File soal" }).status(500);
  }
}

export default new SoalService();
