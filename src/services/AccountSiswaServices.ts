import path from "path";
import xlsx from "node-xlsx";
import jwt from "jsonwebtoken";
import { Request, Response } from "express";
const db = require("../../database/models");

class AccountSiswaService {
  async get(req: Request, res: Response) {
    let data;
    try {
      data = await db.siswaAccount.findAll({});
    } catch (error) {
      console.log(error);
      return res.send(500);
    }
    return res.send({ data }).status(200);
  }

  async getId(req: Request, res: Response) {
    let { id } = req.params;
    let data;
    try {
      data = await db.siswaAccount.findOne({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
      return res.send(500);
    }
    return res.send({ data }).status(200);
  }

  store(req: Request, res: Response) {
    if (req.file) {
      //parsing excel data
      const accountXlsx = xlsx.parse(
        path.join(
          __dirname + "../../../storage/siswaaccount/" + req.file.filename
        )
      );

      //find data
      let data: Array<any> = accountXlsx[0].data;

      //remove first row or field excel
      data.shift();

      //insert data
      try {
        for (let document of data) {
          db.siswaAccount.create({
            nisn: document[0],
            nama: document[1],
            username: document[2],
            password: document[3],
          });
        }
      } catch (error) {
        console.log(error);
        return res.send(500);
      }
      return res.send(200);
    }
    return res.send({ message: "no file upload" }).status(500);
  }

  async update(req: Request, res: Response) {
    let { id } = req.params;
    let { nisn, username, password } = req.body;
    try {
      await db.siswaAccount.update(
        {
          nisn,
          username,
          password,
        },
        {
          where: {
            id,
          },
        }
      );
    } catch (error) {
      console.log(error);
      return res.send(500);
    }
    return res.send(200);
  }

  async destroy(req: Request, res: Response) {
    let { id } = req.params;
    try {
      await db.siswaAccount.destroy({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
      return res.send(500);
    }
    return res.send(200);
  }

  async Authentication(req: Request, res: Response) {
    let { username, password } = req.body;
    let siswaAccount;
    try {
      siswaAccount = await db.siswaAccount.findOne({
        include: [
          {
            model: db.siswa,
            required: true,
          },
        ],
        where: {
          username,
          password,
        },
      });
    } catch (error) {
      console.log(error);
      return res.send(500);
    }
    if (siswaAccount == null) {
      return res.status(404).send({ message: "Account Not Found" });
    }
    const token = await jwt.sign(
      {
        username: siswaAccount.username,
        nisn: siswaAccount.nisn,
        nama: siswaAccount.siswa.nama,
        kelas: siswaAccount.siswa.kelas,
        jurusan: siswaAccount.siswa.jurusan,
      },
      "secret-key"
    );
    return res.status(200).send({ token });
  }
}

export default new AccountSiswaService();
